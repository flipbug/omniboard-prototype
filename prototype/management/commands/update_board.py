import json

from ws4redis.redis_store import RedisMessage
from ws4redis.publisher import RedisPublisher

from django.utils import timezone

from django.core.management.base import BaseCommand
from prototype.models import BoardEntry


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.update_board()

    def update_board(self):
        messages = self.collect_messages()
        redis_message = RedisMessage(json.dumps(messages, indent=4))
        RedisPublisher(facility='general', broadcast=True).publish_message(redis_message)

    @staticmethod
    def collect_messages():
        messages = []
        now = timezone.localtime(timezone.now())
        entries = BoardEntry.objects.filter(
            displaytime__start_date__lte=now,
            displaytime__end_date__gt=now
        )

        for entry in entries:
            messages.append({
                'priority': entry.priority,
                'message': entry.message
            })

        return messages
