from django.views.generic.base import TemplateView
from django.conf import settings


class BoardView(TemplateView):
    template_name = 'prototype/board.html'

    def get(self, request, *args, **kwargs):
        return super(BoardView, self).get(request, *args)
