from django.contrib import admin
from prototype.models import BoardEntry, DisplayTime


class DisplayTimeInline(admin.StackedInline):
    model = DisplayTime
    extra = 0


class BoardEntryAdmin(admin.ModelAdmin):
    inlines = [
        DisplayTimeInline,
    ]

admin.site.register(BoardEntry, BoardEntryAdmin)
