from __future__ import unicode_literals

from django.db import models


class BoardEntry(models.Model):
    PRIO_CHOICES = (
        (0, u'Low'),
        (1, u'Normal'),
        (2, u'High'),
    )
    title = models.CharField(
        max_length=128
    )

    message = models.TextField()

    priority = models.IntegerField(
        choices=PRIO_CHOICES,
        default=0
    )


class DisplayTime(models.Model):
    start_date = models.DateTimeField(
        verbose_name=u'Start Date'
    )

    end_date = models.DateTimeField(
        verbose_name=u'End Date'
    )

    board_entry = models.ForeignKey(
        BoardEntry,
        on_delete=models.CASCADE
    )
