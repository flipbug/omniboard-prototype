/**
 * Created by danieladmin on 14/03/16.
 */

(function() {
	var ws4redis = WS4Redis({
		uri: '{{ WEBSOCKET_URI }}general?subscribe-broadcast&publish-broadcast&echo',
		receive_message: receiveMessage,
		heartbeat_msg: '--heartbeat--'
	});

	// receive a message though the Websocket from the server
	function receiveMessage(msg) {
		billboard.append('<br/>' + msg);
		billboard.scrollTop(billboard.scrollTop() + 25);
	}
});
