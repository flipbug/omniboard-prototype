# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-14 18:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prototype', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='boardentry',
            name='priority',
            field=models.IntegerField(choices=[(0, 'Low'), (1, 'Normal'), (2, 'High')], default=0),
        ),
        migrations.AlterField(
            model_name='displaytime',
            name='end_date',
            field=models.DateTimeField(verbose_name='End Date'),
        ),
    ]
